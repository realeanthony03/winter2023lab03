public class Dog
{
	public String name;
	public String breed;
	public int weight;
	
	public  void dogName(String name)
	{
		System.out.println("the dogs' name is "+ name);
	}
	
	public void breedWeight(String name,String breed, int weight)
	{
		System.out.println(name+" is a "+ breed+" breed and weighs "+ weight + "lbs");
	}
}